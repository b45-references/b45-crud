<?php
	echo form_open('crud/save','class="myform"');
	?>
	<div class="form-group">
		<label for="nm">Full Name</label>
		<input type="text" name="nm" id="nm" class="form-control" placeholder="Nama Panjang"/>
	</div>
	<div class="form-group">
		<label for="em">Email</label>
		<input type="text" name="em" id="em" class="form-control" placeholder="Surel Anda"/>
	</div>
	<div class="form-group">
		<label for="hp">Phone Number</label>
		<input type="text" name="hp" id="hp" class="form-control" placeholder="Nomor HP"/>
	</div>
	<div class="form-group">
		<label for="al">Address</label>
		<input type="text" name="al" id="al" class="form-control" placeholder="Alamat"/>
	</div>
	<input type="submit" name="save" class="btn btn-primary" value="Save"/>
	<a href="<?php echo site_url('crud') ?>" class="btn btn-link">Back View</a>
	<?php
	echo form_close();
?>